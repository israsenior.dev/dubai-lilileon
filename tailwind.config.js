const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  content: ["./src/**/*"],
  darkMode: "media", // or 'media' or 'class'
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
    },
    fontFamily: {
      paragraph: ["IBM Plex Mono", "monospace"]
    },
    extend: {
      colors: {
        primary: "#955F3B",
        secondary: "#D8BEAF",
        accent: "#D93D3D",
        paragraph: "#5E5E5E"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("autoprefixer")],
};
